import React from 'react';
import ContactForm from "../contact/ContactForm";
import {makeStyles} from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import {Link} from "react-router-dom";

const articles = [
    {
        title: 'The best Web Devleoper – Questions & Answers\n',
        date: 'APRIL 23, 2021\n',
        description: 'If you want to hire a web developer right away without reading , click here and send me a message! What is a web developer? Who is actually a...\n'
    },
    {
        title: 'Website design – how to start?\n',
        date: 'MARCH 21, 2021\n',
        description: 'Designing a website starts with defining the role the website is meant to serve, or its purpose. You’re guessing right – there are different types of websites that are...\nIf you want to hire a web developer right away without reading , click here and send me a message! What is a web developer? Who is actually a...\n'
    },
    {
        title: 'Looking for a WordPress expert?\n',
        date: 'MARCH 14, 2021\n',
        description: 'You need a WordPress expert in order to make your website or online store grow? Or maybe you need someone to help you solve a problem with WordPress or...\n'
    },
    {
        title: 'How much does a WordPress website cost?\n',
        date: 'MARCH 13, 2021\n',
        description: 'If you want to hire a WordPress specialist right away without reading , click here and send me a message! When answering the question of how much does a...\n'
    },
    {
        title: 'What Does the Development of Medical Mobile Software Consist of\n',
        date: 'MARCH 9, 2021\n',
        description: 'The need for a telehealth solution has created a huge market for medical mobile software. Understanding what types of options are available is just the start of the development...\n'
    },
    {
        title: 'Optimizing a website – how it impacts SEO?\n',
        date: 'FEBRUARY 26, 2021\n',
        description: 'If you want to optimse your website right away without reading , click here and send me a message! Google likes optimized websites. Your clients like them, too. How...\n'
    },
    {
        title: 'Designing and creating websites for business\n',
        date: 'FEBRUARY 26, 2021\n',
        description: 'Need to create a website for your business? Click here and send me a message! Step 1: why do you need a website? Everyone who decides to create a...\n'
    },
    {
        title: 'The best front-end developer portfolio\n',
        date: 'FEBRUARY 24, 2021\n',
        description: 'If you want to hire a font-end developer right away without reading , click here and send me a message! The graphic designer shares his graphic designs, web designer...\n'
    },
    {
        title: 'First steps with Gulp.js\n',
        date: 'JANUARY 22, 2017\n',
        description: 'Front end front end developers spend little time coding. Even if we ignore meetings, much of the job involves basic tasks your working day: Tasks must be repeated every...\n'
    }
]

const useStyles = makeStyles((theme) => ({
    root: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '& > *': {
            marginTop: theme.spacing(2),
        },
        '& button': {
            '& svg': {
                color: 'white',
                fontSize: '1.7rem',
            },
            color: 'white',
            fontSize: '1.7rem',
        }
    },
}));

function Wall(props) {
    const classes = useStyles();
    return (
        <div className="wall">
            <div className="wall__container">
                <div className="wall__articles" style={{paddingRight: '10rem'}}
                     uk-scrollspy="cls: uk-animation-fade; target: article ; delay: 500; repeat: true">
                    {articles.map((article, index) => (
                        <article key={index} className="wall__article p-right-big">
                            <h1 className="m-left-big secondary-color title-small font-file-space">
                                {article.title}
                            </h1>
                            <h6 className="m-left-big m-top-small m-bottom-medium">
                                {article.date}
                            </h6>
                            <p className="m-left-big lead m-bottom-medium">
                                {article.description}
                            </p>
                            <p className="m-left-big m-bottom-small">
                                <Link to={`/blog/${article.title}`} className="primary-color">
                                    Read More...
                                </Link>
                            </p>
                        </article>
                    ))}
                </div>
                <div className="wall__sidebar">
                    <h2 className="primary-color title-medium font-file-space font-file-space-sm"
                        uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 500">
                        <div className="">
                            {'Tags'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h2>
                    <div className="wall__tags" uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 700">
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                    </div>
                    <h2 className="primary-color title-medium font-file-space font-file-space-sm"
                        uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 500">
                        <div className="">
                            {'Contact Me'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h2>
                    <div uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 800">
                        <ContactForm/>
                    </div>
                </div>
            </div>
            <div className={classes.root}>
                <Pagination count={10} color="secondary"/>
            </div>
        </div>
    );
}

export default Wall;