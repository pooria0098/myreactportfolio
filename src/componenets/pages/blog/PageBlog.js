import React from 'react';
import ContactForm from "../contact/ContactForm";
import {Link} from "react-router-dom";
import Work1 from '../../../assets/w1.jpg';

function PageBlog(props) {
    return (
        <div className="weblog">
            <div className="weblog__container">
                <div className="weblog__article" style={{paddingRight: '10rem'}}>
                    <Link to="/wall" className="primary-color p-left-big"
                          uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 400">Go Back To Wall</Link>
                    <div className="weblog__article--cover m-top-big"
                         uk-scrollspy="cls: uk-animation-slide-top; repeat: true; delay : 400">
                        <img className="p-left-big" src={Work1} alt=""/>
                    </div>
                    <h1 className="title-big p-left-big" style={{color: 'white'}}
                        uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 600">
                        <div className="p-left-big">
                            {'The best Web Developer – Questions & Answers'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h1>
                    <h2 className="primary-color p-left-big title-medium font-file-space font-file-space-sm"
                        uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 800">
                        <div className="p-left-big">
                            {'What is a web developer?'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h2>
                    <p className="lead p-left-big" uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 1000">
                        <span className="p-left-big p-right-big block">
                            Who is actually a web developer? When interpreting the term literally in relation to IT,
                            we could conclude it’s a programmer for websites. It is definitely an IT specialist,
                            which means a programmer, but there is no way to define it precisely, it’s a very general term,
                            and it can only be given a detailed meaning when we specify the scope of tasks or technologies.
                            They are responsible for web applications — mainly for creating them.
                        </span>
                        <span className="p-left-big p-right-big block">
                            A web developer has plenty of career paths, sets of skills and possibilities.
                            In most cases, it’s a front-end type programmer who is responsible for the website visible to the end user,
                            its mechanics, the web app. But there are also IT specialists working at the so-called back-end as well as
                            DevOps that are referred to as web developers. Unlike a web architect, a web developer doesn’t specialize in
                            graphics or aesthetics of WWW on the designing side. Neither is its content itself within the scope of tasks of a
                            front-end developer, the content is taken care of by the client, a copywriter or the person in charge of SEO.
                        </span>
                    </p>
                    <h2 className="primary-color p-left-big title-medium font-file-space font-file-space-sm"
                        uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 800">
                        <div className="p-left-big">
                            {'What are the responsibilities of a web developer?'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h2>
                    <p className="lead p-left-big" uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 1000">
                        <span className="p-left-big p-right-big block">
                            The core of a web developer’s responsibilities is creating the source code of a web app. A web dev often watches over the entirety of a project, taking the role of a technical consultant, analyst and solution architect.
                        </span>
                        <span className="p-left-big p-right-big block">
                            Who is sought after as a web developer? What type of soft and hard skills are required?
                        </span>
                        <span className="p-left-big p-right-big block">
                            Before starting your first job, you need to master coding in HTML, CSS and JS. These three languages are currently the basis of website coding. Due to the changing needs of clients, it’s a good idea to learn one of the programming languages (like PHP).
                        </span>
                        <span className="p-left-big p-right-big block">
                            It is very important for a web dev to be immune to stress and be able to work under time pressure. In this position, the ability to look at the problems at hand in a creative and analytical way is a must. Interpersonal skills are also important. Especially if you want to be part of a team.
                        </span>
                    </p>
                    <Link to="/wall" className="primary-color p-left-big"
                          uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 400">Go Back To Wall</Link>
                </div>
                <div className="weblog__sidebar">
                    <h2 className="primary-color title-medium font-file-space font-file-space-sm"
                        uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 500">
                        <div className="">
                            {'Tags'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h2>
                    <div className="weblog__tags"
                         uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 700">
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                        <p className="lead p-left-big">lorem ipsum text generator</p>
                    </div>
                    <h2 className="primary-color title-medium font-file-space font-file-space-sm"
                        uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 500">
                        <div className="">
                            {'Contact Me'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h2>
                    <div uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 800">
                        <ContactForm/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PageBlog;