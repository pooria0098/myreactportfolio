// import React from 'react'
// import {Button, Image, Modal} from 'semantic-ui-react'
//
// function Modal() {
//     const [open, setOpen] = React.useState(false)
//
//     return (
//         <Modal
//             onClose={() => setOpen(false)}
//             onOpen={() => setOpen(true)}
//             open={open}
//             trigger={<Button>Show Modal</Button>}
//         >
//             <Modal.Header>Upload image</Modal.Header>
//             <Modal.Content image>
//                 <Image size='medium' src='https://react.semantic-ui.com/images/wireframe/image-square.png' wrapped/>
//                 <Modal.Description>
//                     <p>Would you like to upload this image?</p>
//                 </Modal.Description>
//             </Modal.Content>
//             <Modal.Actions>
//                 <Button onClick={() => setOpen(false)}>Cancel</Button>
//                 <Button onClick={() => setOpen(false)} positive>
//                     Ok
//                 </Button>
//             </Modal.Actions>
//         </Modal>
//     )
// }
//
// export default ModalExampleContentImage

import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        width:'500px',
        textAlign:'center'
    },
}));

export default function TransitionsModal(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <button type="button" onClick={handleOpen}>
                View Project
            </button>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <img src={props.img} alt=""/>
                        <h2 id="transition-modal-title">Modal Title</h2>
                        <p id="transition-modal-description">Modal Description</p>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
