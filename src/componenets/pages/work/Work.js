import React from 'react';
import Work1 from '../../../assets/w1.jpg';
import Work2 from '../../../assets/w2.png';
import Work3 from '../../../assets/w3.jpg';
import Work4 from '../../../assets/w4.jpg';
import Work5 from '../../../assets/w5.png';
import Work6 from '../../../assets/w6.jpg';
import Work7 from '../../../assets/w7.jpg';
import Work8 from '../../../assets/w8.jpg';
import Work9 from '../../../assets/w9.png';
import Work10 from '../../../assets/w10.png';
import Work11 from '../../../assets/w11.jpg';
import Work12 from '../../../assets/w12.png';
import Work13 from '../../../assets/w13.jpg';
import Work14 from '../../../assets/w14.jpg';
import Work15 from '../../../assets/w15.jpg';
import Work16 from '../../../assets/w16.jpg';
import Work17 from '../../../assets/w17.jpeg';
import Work18 from '../../../assets/w18.jpeg';
import Modal from "./Modal";

function Work() {
    return (
        <div className="work__container">
            <div uk-slider="autoplay: true;autoplay-interval: 1300;pause-on-hover: true"
                 className="uk-slider uk-slider-container">
                <ul className="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m uk-light"
                    style={{transform: "translate3d(-160px, 0px, 0px)", height: '33%'}}>
                    {[Work1, Work2, Work3, Work4, Work5, Work6].map((work, index) => (
                        <li key={index} className="uk-transition-toggle" tabIndex="-1" style={{order: 1}}>
                            <img src={work} alt="" style={{height: '230px', width: '100%'}}/>
                            <div className="uk-position-center uk-panel">
                                <h1 className="uk-transition-slide-bottom-small" id="uk-transition-slide-bottom-small">
                                    {/*<img src={CircleView} alt="" style={{width: '100px', height: '100px'}}/>*/}
                                    <Modal img={work}/>
                                </h1>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
            <div uk-slider="autoplay: true;autoplay-interval: 1800;pause-on-hover: true"
                 className="uk-slider uk-slider-container">
                <ul className="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m uk-light"
                    style={{transform: "translate3d(-160px, 0px, 0px)", height: '33%'}}>
                    {[Work7, Work8, Work9, Work10, Work11, Work12].map((work, index) => (
                        <li key={index} className="uk-transition-toggle" tabIndex="-1" style={{order: 1}}>
                            <img src={work} alt="" style={{height: '230px', width: '100%'}}/>
                            <div className="uk-position-center uk-panel">
                                <h1 className="uk-transition-slide-bottom-small" id="uk-transition-slide-bottom-small">
                                    {/*<img src={CircleView} alt="" style={{width: '100px', height: '100px'}}/>*/}
                                    <Modal img={work}/>
                                </h1>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
            <div uk-slider="autoplay: true;autoplay-interval: 2900;pause-on-hover: true"
                 className="uk-slider uk-slider-container">
                <ul className="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m uk-light"
                    style={{transform: "translate3d(-160px, 0px, 0px)", height: '33%'}}>
                    {[Work13, Work14, Work15, Work16, Work17, Work18].map((work, index) => (
                        <li key={index} className="uk-transition-toggle" tabIndex="-1" style={{order: 1}}>
                            <img src={work} alt="" style={{height: '230px', width: '100%'}}/>
                            <div className="uk-position-center uk-panel">
                                {/*<h1 className="uk-transition-slide-bottom-small">*/}
                                {/*<img className="uk-transition-slide-bottom-small"*/}
                                {/*     src={CircleView}*/}
                                {/*     alt=""*/}
                                {/*     style={{width: '100px', height: '100px'}}/>*/}
                                {/*</h1>*/}
                                <h1 className="uk-transition-slide-bottom-small" id="uk-transition-slide-bottom-small">
                                    {/*<img src={CircleView} alt="" style={{width: '100px', height: '100px'}}/>*/}
                                    <Modal img={work}/>
                                </h1>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    )
}

export default Work;