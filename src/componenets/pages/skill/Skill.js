import React from 'react';
import {Link} from "react-router-dom";

function Skill() {
    return (
        <section className="skills">
            <div className="skills__left">
                <h2 className="primary-color p-left-big title-medium font-file-space font-file-space-sm"
                    uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 600">
                    <div className="p-left-big">
                        {'Skills & Experience'.split('').map((ch, index) =>
                            ch === ' ' ? ' ' :
                                <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                      style={{opacity: 1}}>{ch}</span>
                        )}
                    </div>
                </h2>
                <p className="lead p-left-big" uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 800">
                <span className="p-left-big p-right-big block">
                Since beginning my journey as a freelance developer nearly 10 years ago, I’ve done remote work for
                agencies, consulted for startups, and collaborated with talented people to create web products for both
                business and consumer use.</span>
                    <span className="p-left-big p-right-big block">
                I create successful responsive websites that are fast, easy to use, and built with best practices. The
                main area of my expertise is front-end development, HTML, CSS, JS, building small and medium web apps,
                custom plugins, features, animations, and coding interactive layouts.</span>
                    <span className="p-left-big p-right-big block">
                I also have full-stack developer experience with popular open-source CMS like (WordPress, Drupal,
                Magento, Keystone.js and others) .Visit my <a href="/#" className="primary-color">LinkedIn</a> profile for more details or just <Link
                        to="/contact" className="primary-color">contact</Link> me.</span>
                </p>
            </div>
            <div className="skills__right p-top-big">
                <div className="skills__items" uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 700">
                    <div className="skills__item">
                        <p>Front-end</p>
                        <div className="progress">
                            <div style={{width: "100%"}}/>
                        </div>
                    </div>
                    <div className="skills__item">
                        <p>Back-end</p>
                        <div className="progress">
                            <div style={{width: "90%"}}/>
                        </div>
                    </div>
                    <div className="skills__item">
                        <p>Django</p>
                        <div className="progress">
                            <div style={{width: "80%"}}/>
                        </div>
                    </div>
                    <div className="skills__item">
                        <p>React</p>
                        <div className="progress">
                            <div style={{width: "70%"}}/>
                        </div>
                    </div>
                </div>
                <div className="skills__boxes" uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 900">
                    <div className="skills__box">
                        <h1 style={{color: 'white'}}>Frontend developer</h1>
                        <h4 className="text-gray">To The End</h4>
                        <h6 className="text-gray">2017-2018</h6>
                        <p className="lead text-gray">Award-winning Content Marketing Agency specialises in
                            creating and sharing engaging
                            content.</p>
                    </div>
                    <div className="skills__box">
                        <h1 style={{color: 'white'}}>Full stack developer</h1>
                        <h4 className="text-gray">Pixels Digital</h4>
                        <h6 className="text-gray">2018-2021</h6>
                        <p className="lead text-gray">Creative brand, website design and development studio that
                            can bring your online business
                            dreams
                            to life</p>
                    </div>
                </div>
            </div>

        </section>
    );
}

export default Skill;