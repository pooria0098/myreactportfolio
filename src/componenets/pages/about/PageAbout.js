import React from 'react';
import About from "../home/About";
// import {Link} from "react-router-dom";
// import About from "../About";

function PageAbout() {
    return (
        <About/>
        // <section className="about">
        //     <div className="about__left-box">
        //         <h2 className="primary-color p-left-big title-medium font-file-space font-file-space-sm">
        //             <div className="p-left-big">
        //                 {'Me, Myself and I'.split('').map((ch, index) =>
        //                     ch === ' ' ? ' ' :
        //                         <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
        //                               style={{opacity: 1}}>{ch}</span>
        //                 )}
        //             </div>
        //         </h2>
        //         <p className="lead p-left-big">
        //                     <span className="p-left-big p-right-big block">
        //                     The primary area of my interest is front-end. My passion for code has begun when Adobe Flash was a
        //                     complete innovation, alongside with CSS 1.0 and HTML 4.01 as standards of the current web.
        //                     </span>
        //             <span className="p-left-big p-right-big block">
        //                     For over a decade I had many opportunities to work in a vast spectrum of
        //                     <Link to="/skills" className="primary-color"> web technologies</Link> what let
        //                     me gather a significant amount of various experience. Working for companies and individuals around the
        //                     globe I met and learnt from amazing and ambitious people.
        //                     </span>
        //             <span className="p-left-big p-right-big block">
        //                     I currently work remotely with a selected freelance client base and are open for new opportunities.
        //                     </span>
        //         </p>
        //     </div>
        //     <div className="about__right-box">
        //
        //     </div>
        // </section>
    );
}

export default PageAbout;