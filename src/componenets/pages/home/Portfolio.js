import React from 'react';
import backGroundImgSrc from '../../../assets/907e2c4040fcb95a54e3414f3bdf5e3a.png';
import Work1 from '../../../assets/w1.jpg';
import Work2 from '../../../assets/w2.png';
import Work3 from '../../../assets/w3.jpg';
import Work4 from '../../../assets/w4.jpg';
import Work5 from '../../../assets/w5.png';
import Work6 from '../../../assets/w6.jpg';
import Work7 from '../../../assets/w7.jpg';
import Work8 from '../../../assets/w8.jpg';
import Work9 from '../../../assets/w9.png';
import Work10 from '../../../assets/w10.png';
import {Link} from 'react-router-dom';
import Modal from '../work/Modal';

function Portfolio() {
    return (
        <div className="portfolio">
            <div className="portfolio__cover">
                {/*<div className="portfolio__cover--left">&nbsp;</div>*/}
                {/*<div className="portfolio__cover--right">*/}
                <img src={backGroundImgSrc} alt="Work"/>
                {/*</div>*/}
            </div>
            <h2 className="primary-color p-left-big title-medium font-file-space font-file-space-sm"
                uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 600">
                <div className="p-left-big">
                    {'My Portfolio'.split('').map((ch, index) =>
                        ch === ' ' ? ' ' :
                            <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                  style={{opacity: 1}}>{ch}</span>
                    )}
                </div>
            </h2>
            <p className="lead p-left-big" style={{width: '1000px'}}
               uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 800">
                <span className="p-left-big p-right-big">
                    A small gallery of recent projects chosen by me. I've done them all together
                    with amazing people from companies around the globe. It's only a drop in the
                    ocean compared to the entire list.Interested to see some more? Visit &nbsp;
                    <Link to="/work" className="primary-color">my work</Link> page.
                </span>
            </p>
            <Link to="/work" uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 1000">
                <div className="header__btn button-container-2 m-left-big m-bottom-big">
                    <span className="mas">See More</span>
                    <button type="button" name="Hover">See More</button>
                </div>
            </Link>

            <div className="context m-top-big">
                <section className="portfolio__gallery">
                    {
                        [Work1, Work2, Work3, Work4, Work5, Work6, Work7, Work8, Work9, Work10].map((work, index) => (
                            <div key={index} className="portfolio__container">
                                <div className="portfolio__box">
                                    <img className="portfolio__img" src={work} alt="project-pic"/>
                                </div>
                                <div className="portfolio__overlay">
                                    <Modal img={work}/>
                                    {/*<div className="portfolio__overlay-popup">*/}
                                    {/*    <span className="block p-left-xsmall">View</span>*/}
                                    {/*    <span className="block">Project</span>*/}
                                    {/*</div>*/}
                                </div>
                            </div>
                        ))
                    }
                </section>
            </div>
        </div>
    );
}

export default Portfolio;