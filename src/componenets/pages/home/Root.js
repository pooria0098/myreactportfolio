import React from 'react';
import Header from "./Header";
import Portfolio from "./Portfolio";
import About from "./About";
import Blog from "./Blog";
import Contact from "../contact/Contact";

function Root() {
    return (
        <React.Fragment>
            <Header/>
            <Portfolio/>
            <About/>
            <Blog/>
            <Contact/>
        </React.Fragment>
    );
}

export default Root;