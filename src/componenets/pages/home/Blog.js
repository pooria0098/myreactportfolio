import React from 'react';
import BlogImg from '../../../assets/3c709fb4c55d744c9d504c555640cd1c.png';
import {Link} from "react-router-dom";

const articles = [
    {
        title: 'The best Web Developer – Questions & Answers\n',
        date: 'APRIL 23, 2021\n',
        description: 'If you want to hire a web developer right away without reading , click here and send me a message! What is a web developer? Who is actually a...\n'
    },
    {
        title: 'Website design – how to start?\n',
        date: 'MARCH 21, 2021\n',
        description: 'Designing a website starts with defining the role the website is meant to serve, or its purpose. You’re guessing right – there are different types of websites that are...\nIf you want to hire a web developer right away without reading , click here and send me a message! What is a web developer? Who is actually a...\n'
    },
    {
        title: 'Looking for a WordPress expert?\n',
        date: 'MARCH 14, 2021\n',
        description: 'You need a WordPress expert in order to make your website or online store grow? Or maybe you need someone to help you solve a problem with WordPress or...\n'
    },
    {
        title: 'How much does a WordPress website cost?\n',
        date: 'MARCH 13, 2021\n',
        description: 'If you want to hire a WordPress specialist right away without reading , click here and send me a message! When answering the question of how much does a...\n'
    }
]

function Blog() {
    return (
        <div className="blog" style={{marginTop: '30rem'}}>
            <div className="blog__overlay p-left-big p-right-big">
                <img src={BlogImg} alt="blog"/>
            </div>
            <section className="blog__context m-top-big">
                {articles.map((article, index) => (
                    <article key={index} className="blog__article" uk-scrollspy="cls: uk-animation-slide-bottom; repeat: true; delay: 1000">
                        <h1 className="m-left-big secondary-color title-small font-file-space">
                            {article.title}
                        </h1>
                        <h6 className="m-left-big m-top-small m-bottom-medium">
                            {article.date}
                        </h6>
                        <p className="m-left-big lead m-bottom-medium">
                            {article.description}
                        </p>
                        <p className="m-left-big m-bottom-small">
                            <Link to={`/blog/${article.title}`} className="primary-color">
                                Read More...
                            </Link>
                        </p>
                    </article>
                ))}
            </section>
            <div className="blog__read-more m-right-big p-right-big" uk-scrollspy="cls: uk-animation-slide-right; repeat: true">
                <Link to="/wall" className="primary-color">
                    Read More Articles....
                </Link>
            </div>
        </div>
    );
}

export default Blog;