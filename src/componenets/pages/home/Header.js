import React, {useEffect} from 'react';
import PImg from '../../../assets/GoldTypographyP.png'
import {Link} from "react-router-dom";


function Header() {
    useEffect(() => {
        // Class
        class TypeWriter {
            constructor(txtElement, words, wait = 3000) {
                this.txtElement = txtElement;
                this.words = words;
                this.txt = '';
                this.wordIndex = 0;
                this.wait = parseInt(wait, 10);
                this.type();
                this.isDeleting = false;
            }

            type() {
                // Current index of word
                const current = this.wordIndex % this.words.length;
                // Get full text of current word
                const fullTxt = this.words[current];

                // Check if deleting
                if (this.isDeleting) {
                    // Remove char
                    this.txt = fullTxt.substring(0, this.txt.length - 1);
                } else {
                    // Add char
                    this.txt = fullTxt.substring(0, this.txt.length + 1);
                }

                // Insert txt into element
                this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;

                // Initial Type Speed
                let typeSpeed = 300;

                if (this.isDeleting) {
                    typeSpeed /= 2;
                }

                // If word is complete
                if (!this.isDeleting && this.txt === fullTxt) {
                    // Make pause at end
                    typeSpeed = this.wait;
                    // Set delete to true
                    this.isDeleting = true;
                } else if (this.isDeleting && this.txt === '') {
                    this.isDeleting = false;
                    // Move to next word
                    this.wordIndex++;
                    // Pause before start typing
                    typeSpeed = 500;
                }

                setTimeout(() => this.type(), typeSpeed);
            }
        }

        // Init On DOM Load
        document.addEventListener('DOMContentLoaded', init);

        // Init App
        function init() {
            const txtElement = document.querySelector('.txt-type');
            const words = JSON.parse(txtElement.getAttribute('data-words'));
            const wait = txtElement.getAttribute('data-wait');
            // Init TypeWriter
            new TypeWriter(txtElement, words, wait);
        }

        // Canvas
        var c = document.getElementById("c")
        var w = c.width = window.innerWidth - 225,
            h = c.height = window.innerHeight,
            ctx = c.getContext('2d'),

            opts = {

                len: 20,
                count: 50,
                baseTime: 10,
                addedTime: 10,
                dieChance: .05,
                spawnChance: 1,
                sparkChance: .1,
                sparkDist: 10,
                sparkSize: 2,

                color: 'hsl(hue,100%,light%)',
                baseLight: 50,
                addedLight: 10, // [50-10,50+10]
                shadowToTimePropMult: 6,
                baseLightInputMultiplier: .01,
                addedLightInputMultiplier: .02,

                cx: w / 2,
                cy: h / 2,
                repaintAlpha: .04,
                hueChange: .1
            },

            tick = 0,
            lines = [],
            dieX = w / 2 / opts.len,
            dieY = h / 2 / opts.len,

            baseRad = Math.PI * 2 / 6;

        ctx.fillStyle = 'black';
        ctx.fillRect(0, 0, w, h);

        function loop() {

            window.requestAnimationFrame(loop);

            ++tick;

            ctx.globalCompositeOperation = 'source-over';
            ctx.shadowBlur = 0;
            ctx.fillStyle = 'rgba(39,39,39,alp)'.replace('alp', opts.repaintAlpha);
            ctx.fillRect(0, 0, w, h);
            ctx.globalCompositeOperation = 'lighter';

            if (lines.length < opts.count && Math.random() < opts.spawnChance)
                lines.push(new Line());

            // eslint-disable-next-line array-callback-return
            lines.map(function (line) {
                line.step();
            });
        }

        function Line() {

            this.reset();
        }

        Line.prototype.reset = function () {

            this.x = 0;
            this.y = 0;
            this.addedX = 0;
            this.addedY = 0;

            this.rad = 0;

            this.lightInputMultiplier = opts.baseLightInputMultiplier + opts.addedLightInputMultiplier * Math.random();

            this.color = opts.color.replace('hue', tick * opts.hueChange);
            this.cumulativeTime = 0;

            this.beginPhase();
        }
        Line.prototype.beginPhase = function () {

            this.x += this.addedX;
            this.y += this.addedY;

            this.time = 0;
            this.targetTime = (opts.baseTime + opts.addedTime * Math.random()) | 0;

            this.rad += baseRad * (Math.random() < .5 ? 1 : -1);
            this.addedX = Math.cos(this.rad);
            this.addedY = Math.sin(this.rad);

            if (Math.random() < opts.dieChance || this.x > dieX || this.x < -dieX || this.y > dieY || this.y < -dieY)
                this.reset();
        }
        Line.prototype.step = function () {

            ++this.time;
            ++this.cumulativeTime;

            if (this.time >= this.targetTime)
                this.beginPhase();

            var prop = this.time / this.targetTime,
                wave = Math.sin(prop * Math.PI / 2),
                x = this.addedX * wave,
                y = this.addedY * wave;

            ctx.shadowBlur = prop * opts.shadowToTimePropMult;
            ctx.fillStyle = ctx.shadowColor = this.color.replace('light', opts.baseLight + opts.addedLight * Math.sin(this.cumulativeTime * this.lightInputMultiplier));
            ctx.fillRect(opts.cx + (this.x + x) * opts.len, opts.cy + (this.y + y) * opts.len, 2, 2);

            if (Math.random() < opts.sparkChance)
                ctx.fillRect(opts.cx + (this.x + x) * opts.len + Math.random() * opts.sparkDist * (Math.random() < .5 ? 1 : -1) - opts.sparkSize / 2, opts.cy + (this.y + y) * opts.len + Math.random() * opts.sparkDist * (Math.random() < .5 ? 1 : -1) - opts.sparkSize / 2, opts.sparkSize, opts.sparkSize)
        }
        loop();

        window.addEventListener('resize', function () {

            w = c.width = window.innerWidth;
            h = c.height = window.innerHeight;
            ctx.fillStyle = 'black';
            ctx.fillRect(0, 0, w, h);

            opts.cx = w / 2;
            opts.cy = h / 2;

            dieX = w / 2 / opts.len;
            dieY = h / 2 / opts.len;
        });
    })

    return (
        <header className="header">
            <canvas id="c" className="m-right-big p-right-big"/>
            <div className="header__container">
                <h1 className="title-big header__typo p-left-big" style={{color: 'white'}}
                    uk-scrollspy="cls: uk-animation-slide-top; delay: 200; repeat: true">
                    <div className="p-left-big">
                        {'Hi,'.split('').map((ch, index) =>
                            ch === ' ' ? ' ' :
                                <span key={index} className="blast bounceIn" aria-hidden="true"
                                      style={{opacity: 1}}>{ch}</span>
                        )}
                    </div>
                    <div className="p-left-big">
                        {'I\'m Pooria,'.split('').map((ch, index) =>
                            ch === 'P' ? <img key={index} src={PImg} width='65px' alt="P"/> :
                                <span key={index} className="blast bounceIn" aria-hidden="true"
                                      style={{opacity: 1}}>{ch}</span>
                        )}
                    </div>
                    <div className="p-left-big">
                        {'FullStack Developer'.split('').map((ch, index) =>
                            ch === ' ' ? ' ' :
                                <span key={index} className="blast bounceIn" aria-hidden="true"
                                      style={{opacity: 1}}>{ch}</span>
                        )}
                    </div>
                </h1>
                <p className="header__para text-gray p-left-big"
                   uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 400">
                    <span className="p-left-big">I Am 21 Years Old, I Am A &nbsp;
                        <span className="primary-color lato-italic txt-type" data-wait="3000"
                              data-words='["Front End Developer", "Back End Developer", "Software Engineer"]'/>
                    </span>
                </p>
                <Link to="/contact" className="header__btn">
                    <div className="button-container-2 m-left-big"
                         uk-scrollspy="cls: uk-animation-slide-bottom; repeat: true; delay : 600">
                        <span className="mas">Contact Me</span>
                        <button type="button" name="Hover">Contact Me</button>
                    </div>
                </Link>
                <div className="header__scroll">
                    <div className="header__scrollDown header__scrollDown--left">scroll down &rarr;</div>
                    <div className="header__scrollDown header__scrollDown--right">scroll down &rarr;</div>
                </div>
            </div>
            {/*<canvas style={{position: "absolute", left: 0, zIndex: -1}} id="canvas" width="1094" height="734"/>*/}
        </header>
    );
}

export default Header;