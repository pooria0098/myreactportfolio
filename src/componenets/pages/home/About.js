import React, {useEffect} from 'react';
import {Link} from "react-router-dom";

function About() {
    useEffect(() => {
        // https://codepen.io/team/basedesign/pen/NoPZyQ
        // Get the canvas element from the DOM
        const canvas = document.querySelector('#scene');
        canvas.width = canvas.clientWidth;
        canvas.height = canvas.clientHeight;
        // Store the 2D context
        const ctx = canvas.getContext('2d');

        if (window.devicePixelRatio > 1) {
            canvas.width = canvas.clientWidth * 2;
            canvas.height = canvas.clientHeight * 2;
            ctx.scale(2, 2);
        }

        /* ====================== */
        /* ====== VARIABLES ===== */
        /* ====================== */
        let width = canvas.clientWidth; // Width of the canvas
        let height = canvas.clientHeight; // Height of the canvas
        let rotation = 0; // Rotation of the globe
        let dots = []; // Every dots in an array

        /* ====================== */
        /* ====== CONSTANTS ===== */
        /* ====================== */
        /* Some of those constants may change if the user resizes their screen but I still strongly believe they belong to the Constants part of the variables */
        const DOTS_AMOUNT = 20; // Amount of dots on the screen
        const DOT_RADIUS = 4; // Radius of the dots
        let GLOBE_RADIUS = width * 0.7; // Radius of the globe
        let GLOBE_CENTER_Z = -GLOBE_RADIUS; // Z value of the globe center
        let PROJECTION_CENTER_X = width / 2; // X center of the canvas HTML
        let PROJECTION_CENTER_Y = height / 2; // Y center of the canvas HTML
        let FIELD_OF_VIEW = width * 0.8;

        class Dot {
            constructor(x, y, z) {
                this.x = x;
                this.y = y;
                this.z = z;

                this.xProject = 0;
                this.yProject = 0;
                this.sizeProjection = 0;
            }

            // Do some math to project the 3D position into the 2D canvas
            project(sin, cos) {
                const rotX = cos * this.x + sin * (this.z - GLOBE_CENTER_Z);
                const rotZ = -sin * this.x + cos * (this.z - GLOBE_CENTER_Z) + GLOBE_CENTER_Z;
                this.sizeProjection = FIELD_OF_VIEW / (FIELD_OF_VIEW - rotZ);
                this.xProject = (rotX * this.sizeProjection) + PROJECTION_CENTER_X;
                this.yProject = (this.y * this.sizeProjection) + PROJECTION_CENTER_Y;
            }

            // Draw the dot on the canvas
            draw(text, sin, cos) {
                this.project(sin, cos);
                // ctx.fillRect(this.xProject - DOT_RADIUS, this.yProject - DOT_RADIUS, DOT_RADIUS * 2 * this.sizeProjection, DOT_RADIUS * 2 * this.sizeProjection);
                ctx.beginPath();

                ctx.arc(this.xProject, this.yProject, DOT_RADIUS * this.sizeProjection, 0, Math.PI * 2);
                ctx.closePath();
                ctx.font = '25px serif';
                ctx.fillStyle = "#08fdd8";
                ctx.fillText(text, this.xProject, this.yProject);
            }
        }

        function createDots() {
            // Empty the array of dots
            dots.length = 0;

            // Create a new dot based on the amount needed
            for (let i = 0; i < DOTS_AMOUNT; i++) {
                const theta = Math.random() * 2 * Math.PI; // Random value between [0, 2PI]
                const phi = Math.acos((Math.random() * 2) - 1); // Random value between [-1, 1]

                // Calculate the [x, y, z] coordinates of the dot along the globe
                const x = GLOBE_RADIUS * Math.sin(phi) * Math.cos(theta);
                const y = GLOBE_RADIUS * Math.sin(phi) * Math.sin(theta);
                const z = (GLOBE_RADIUS * Math.cos(phi)) + GLOBE_CENTER_Z;
                dots.push(new Dot(x, y, z));
                // dots.push('A');
            }
        }

        /* ====================== */
        /* ======== RENDER ====== */

        /* ====================== */
        function render(a) {
            // Clear the scene
            ctx.clearRect(0, 0, width, height);

            // Increase the globe rotation
            rotation = a * 0.0004;

            const sineRotation = Math.sin(rotation); // Sine of the rotation
            const cosineRotation = Math.cos(rotation); // Cosine of the rotation

            // console.log('DOTS: ',dots)
            // Loop through the dots array and draw every dot
            const mySkills = ['HTML', 'CSS', 'SASS', 'Bootstrap', 'Semantic UI',
                'Material UI', 'JS', 'ReactJs', 'Git', 'Linux', 'Ubuntu',
                'SQL', 'Sqlite3', 'Postgres', 'Django', 'Python', 'REST',
                'DRF', 'Java', 'Cpp']
            for (var i = 0; i < dots.length; i++) {
                dots[i].draw(mySkills[i], sineRotation, cosineRotation);
            }

            window.requestAnimationFrame(render);
        }


        // Function called after the user resized its screen
        function afterResize() {
            width = canvas.offsetWidth;
            height = canvas.offsetHeight;
            if (window.devicePixelRatio > 1) {
                canvas.width = canvas.clientWidth * 2;
                canvas.height = canvas.clientHeight * 2;
                ctx.scale(2, 2);
            } else {
                canvas.width = width;
                canvas.height = height;
            }
            GLOBE_RADIUS = width * 0.7;
            GLOBE_CENTER_Z = -GLOBE_RADIUS;
            PROJECTION_CENTER_X = width / 2;
            PROJECTION_CENTER_Y = height / 2;
            FIELD_OF_VIEW = width * 0.8;

            createDots(); // Reset all dots
        }

        // Variable used to store a timeout when user resized its screen
        let resizeTimeout;

        // Function called right after user resized its screen
        function onResize() {
            // Clear the timeout variable
            window.clearTimeout(resizeTimeout);
            // Store a new timeout to avoid calling afterResize for every resize event
            resizeTimeout = window.setTimeout(afterResize, 500);
        }

        window.addEventListener('resize', onResize);

        // Populate the dots array with random dots
        createDots();

        // Render the scene
        window.requestAnimationFrame(render);
    })
    return (
        <>
            <section className="about">
                <div className="about__left-box">
                    <h2 className="primary-color p-left-big title-medium font-file-space font-file-space-sm"
                    uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 800">
                        <div className="p-left-big">
                            {'Me, Myself and I'.split('').map((ch, index) =>
                                ch === ' ' ? ' ' :
                                    <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                          style={{opacity: 1}}>{ch}</span>
                            )}
                        </div>
                    </h2>
                    <p className="lead p-left-big" uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 1000">
                    <span className="p-left-big p-right-big block">
                    The primary area of my interest is front-end. My passion for code has begun when Adobe Flash was a
                    complete innovation, alongside with CSS 1.0 and HTML 4.01 as standards of the current web.
                    </span>
                        <span className="p-left-big p-right-big block">
                    For over a decade I had many opportunities to work in a vast spectrum of
                    <Link to="/skills" className="primary-color"> web technologies</Link> what let
                    me gather a significant amount of various experience. Working for companies and individuals around the
                    globe I met and learnt from amazing and ambitious people.
                    </span>
                        <span className="p-left-big p-right-big block">
                    I currently work remotely with a selected freelance client base and are open for new opportunities.
                    </span>
                    </p>
                    <img className="p-left-big" height="115" style={{maxWidth: '100%', marginTop: '40px'}}
                         uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 800"
                         data-src="https://jacekjeznach.com/wp-content/themes/new/img/timeline.png" alt=""
                         src="https://jacekjeznach.com/wp-content/themes/new/img/timeline.png"/>
                </div>
                <div className="about__right-box" uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 1200">
                    <canvas className='canvas-skills' id="scene"/>
                </div>
            </section>
            <div className="header__scroll moveUp">
                <div className="header__scrollDown header__scrollDown--left">scroll down &rarr;</div>
                <div className="header__scrollDown header__scrollDown--right">scroll down &rarr;</div>
            </div>
        </>
    );
}

export default About;