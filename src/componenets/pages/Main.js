import React from 'react';
import Root from "./home/Root";
import PageAbout from "./about/PageAbout";
import Contact from "./contact/Contact";
import Skill from "./skill/Skill";
import Work from './work/Work';
import Weblog from './blog/PageBlog';
import Wall from './blog/Wall';
import PImg from '../../assets/GoldTypographyP.png'
import {NavLink, Link, Redirect, Route, Switch} from "react-router-dom";
import "uikit/dist/css/uikit.min.css";
import "uikit/dist/js/uikit.min.js";
import "uikit/dist/js/uikit-icons.min";

import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles, useTheme} from '@material-ui/core/styles';

import MenuIcon from '@material-ui/icons/Menu';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import InfoIcon from '@material-ui/icons/Info';
import WorkIcon from '@material-ui/icons/Work';
import BookIcon from '@material-ui/icons/Book';
import ContactsIcon from '@material-ui/icons/Contacts';
import LocalAirportIcon from '@material-ui/icons/LocalAirport';

const drawerWidth = 160;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        // backgroundColor: '#1d1d1d',
        // color: 'white'
    },
    drawer: {
        [theme.breakpoints.up('md')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        backgroundColor: '#1d1d1d',
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: '#1d1d1d',
        color: 'white',
        marginTop: '5.5vh',
        padding: theme.spacing(3),
    },
}));

function Main(props) {
    const {window} = props;
    const classes = useStyles();
    const theme = useTheme();
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div className="aside">
            {/*<div className={classes.toolbar}/>*/}
            <div className="text-center">
                <Link to='/'>
                    <img className="logo m-bottom-small" src={PImg} alt=""/>
                    <Typography gutterBottom variant={"h2"} className="white-color">Pooria</Typography>
                    <Typography className="white-color" variant={"h4"}>Web Developer</Typography>
                </Link>
            </div>
            <List>
                <Divider/>
                <ListItem button className="item">
                    <NavLink exact className="item__link" activeClassName="active" to="/about">
                        <ListItemIcon className="icon"><InfoIcon/></ListItemIcon>
                        <ListItemText className="text" primary={"About"}/>
                    </NavLink>
                </ListItem>
                <Divider/>
                <ListItem button className="item">
                    <NavLink exact className="item__link" activeClassName="active" to="/skills">
                        <ListItemIcon className="icon"><LocalAirportIcon/></ListItemIcon>
                        <ListItemText className="text" primary={"My Skills"}/>
                    </NavLink>
                </ListItem>
                <Divider/>
                <ListItem button className="item">
                    <NavLink exact className="item__link" activeClassName="active" to="/work">
                        <ListItemIcon className="icon"><WorkIcon/></ListItemIcon>
                        <ListItemText className="text" primary={"Work"}/>
                    </NavLink>
                </ListItem>
                <Divider/>
                <ListItem button className="item">
                    <NavLink exact className="item__link" activeClassName="active" to="/wall">
                        <ListItemIcon className="icon"><BookIcon/></ListItemIcon>
                        <ListItemText className="text" primary={"Blog"}/>
                    </NavLink>
                </ListItem>
                <Divider/>
                <ListItem button className="item">
                    <NavLink exact className="item__link" activeClassName="active" to="/contact">
                        <ListItemIcon className="icon"><ContactsIcon/></ListItemIcon>
                        <ListItemText className="text" primary={"Contact"}/>
                    </NavLink>
                </ListItem>
                <Divider/>
            </List>
            <List>
                <ListItem className="social">
                    <LinkedInIcon className="socialIcon"/>
                    <FacebookIcon className="socialIcon"/>
                    <TwitterIcon className="socialIcon"/>
                    <InstagramIcon className="socialIcon"/>
                </ListItem>
            </List>
        </div>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <div className={classes.root}>
            <CssBaseline/>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon/>
                    </IconButton>
                    {/*<Typography variant="h6" noWrap className="text-center">*/}
                    <div className="p-left-big secondary-color m-left-big font-file-space font-file-space-md"
                         style={{fontSize: '3rem', fontWeight: 500}}>
                        {'Hi, I\'m Pooria, FrontEnd Developer, BackEnd Developer, FullStack Developer.'.split('').map((ch, index) =>
                            ch === ' ' ? ' ' :
                                <span key={index} className="blast bounceIn" aria-hidden="true"
                                      style={{opacity: 1}}>{ch}</span>
                        )}
                    </div>
                    {/*</Typography>*/}
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        container={container}
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden smDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={`${classes.content} content`}>
                <div className="content-body">
                    {/*<div className={classes.toolbar}/>*/}

                    <Switch>
                        <Route exact path='/' component={Root}/>
                        <Route exact path='/about' component={PageAbout}/>
                        <Route exact path='/skills' component={Skill}/>
                        <Route exact path='/work' component={Work}/>
                        <Route exact path='/wall' component={Wall}/>
                        <Route exact path='/blog/:name' component={Weblog}/>
                        <Route exact path='/contact' component={Contact}/>
                        <Route render={() => <Redirect to="/"/>}/>
                    </Switch>
                </div>
            </main>
        </div>
    );
}

export default Main;