import React, {useState} from 'react';
//https://medium.com/@allynak/how-to-use-google-map-api-in-react-app-edb59f64ac9d
import {GoogleMap, LoadScript, Marker, InfoWindow} from '@react-google-maps/api';

const mapStyles = {
    width: '100%',
    height: '100%'
}

const defaultCenter = {
    lat: 41.3851, lng: 2.1734
}

const locations = [
    {
        name: "You Are In Location 1",
        location: {
            lat: 41.3954,
            lng: 2.162
        },
    },
    {
        name: "You Are In Location 2",
        location: {
            lat: 41.3917,
            lng: 2.1649
        },
    },
    {
        name: "You Are In Location 3",
        location: {
            lat: 41.3773,
            lng: 2.1585
        },
    },
    {
        name: "You Are In Location 4",
        location: {
            lat: 41.3797,
            lng: 2.1682
        },
    },
    {
        name: "You Are In Location 5",
        location: {
            lat: 41.4055,
            lng: 2.1915
        },
    }
];

function Map(props) {
    const [selected, setSelected] = useState({});
    const onSelect = item => {
        setSelected(item);
    }
    return (
        <LoadScript
            googleMapsApiKey='AIzaSyDO7mmAxPlQ_BeQeMCJOo70nOywbQGoBGc'>
            <GoogleMap
                mapContainerStyle={mapStyles}
                zoom={13}
                center={defaultCenter}>
                {locations.map(item => {
                    return (
                        <Marker
                            key={item.name}
                            position={item.location}
                            onClick={() => onSelect(item)}
                        />
                    )
                })}
                {selected.location && (
                    <InfoWindow
                        position={selected.location}
                        clickable={true}
                        onCloseClick={() => setSelected({})}>
                        <p className="showStatus">{selected.name}</p>
                    </InfoWindow>
                )}
            </GoogleMap>
        </LoadScript>
    );
}

export default Map;