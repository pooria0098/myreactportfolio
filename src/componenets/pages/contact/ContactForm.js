import React from 'react';
import useForm from "../../hooks/useForm";

function ContactForm() {
    const [name, setName, resetName] = useForm('')
    const [email, setEmail, resetEmail] = useForm('')
    const [subject, setSubject, resetSubject] = useForm('')
    const [message, setMessage, resetMessage] = useForm('')

    const onFormSubmit = (event) => {
        event.preventDefault()
        const dataSent = {
            name: name,
            email: email,
            subject: subject,
            message: message
        }
        localStorage.setItem('form', JSON.stringify(dataSent))
        resetName()
        resetEmail()
        resetSubject()
        resetMessage()
    }

    const onNameChange = (event) => {
        setName(event)
    }
    const onEmailChange = (event) => {
        setEmail(event)
    }
    const onSubjectChange = (event) => {
        setSubject(event)
    }
    const onMessageChange = (event) => {
        setMessage(event)
    }
    return (
        <form onSubmit={onFormSubmit} className="form">
            <div className="form__double-group p-left-big p-right-big">
                <div className="form__group">
                    <input
                        type="text"
                        name="name"
                        placeholder="Name"
                        id="name"
                        value={name}
                        onChange={onNameChange}
                        className="form__field form__name m-right-medium"
                    />
                    <label htmlFor="name" className="form__label">Name</label>
                </div>
                <div className="form__group">
                    <input
                        required
                        type="email"
                        name="email"
                        placeholder="Email"
                        id="email"
                        value={email}
                        onChange={onEmailChange}
                        className="form__field form__email"
                    />
                    <label htmlFor="email" className="form__label">Email</label>
                </div>
            </div>
            <div className="form__group p-left-big p-right-big">
                <input
                    type="text"
                    name="subject"
                    placeholder="Subject"
                    id="subject"
                    value={subject}
                    onChange={onSubjectChange}
                    className="form__field form__subject"
                />
                <label htmlFor="subject" className="form__label">Subject</label>
            </div>
            <div className="form__group p-left-big p-right-big">
                <textarea
                    required
                    name="message"
                    id="message"
                    rows="7"
                    placeholder="Message"
                    // cols="40"
                    value={message}
                    onChange={onMessageChange}
                    className="form__field form__message"/>
            </div>
            <div className="form__btn button-container-2 m-left-big"
                 style={{marginTop: 0}}>
                <span className="mas">Send</span>
                <button type="submit" name="Hover">Send</button>
            </div>
        </form>
    );
}

export default ContactForm;