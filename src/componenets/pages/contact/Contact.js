import React from 'react';
import ContactForm from "./ContactForm";
import Map from './Map';


function Contact() {
    return (
        <section className="contact m-top-big">
            <div className="contact__text m-right-big">
                <h2 className="primary-color p-left-big title-medium font-file-space font-file-space-sm"
                uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 600">
                    <div className="p-left-big">
                        {'Contact Me'.split('').map((ch, index) =>
                            ch === ' ' ? ' ' :
                                <span key={index} className="blast-reverse bounceIn" aria-hidden="true"
                                      style={{opacity: 1}}>{ch}</span>
                        )}
                    </div>
                </h2>
                <p className="lead p-left-big" uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 800">
                    <span className="p-left-big">
                        I’m interested in freelance opportunities – especially ambitious
                        or large projects. However, if you have other request or question,
                        don’t hesitate to use the form.
                    </span>
                </p>
                <div className="contact__form p-left-big" uk-scrollspy="cls: uk-animation-slide-left; repeat: true; delay : 1000">
                    <ContactForm/>
                </div>
            </div>
            <div className="contact__map" uk-scrollspy="cls: uk-animation-slide-right; repeat: true; delay : 800">
                {/*Todo:overlay don't work*/}
                {/*<div className="contact__map-overlay">*/}
                {/*    &nbsp;*/}
                {/*</div>*/}
                <Map/>
            </div>
        </section>
    );
}

export default Contact;