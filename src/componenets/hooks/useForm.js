import {useState} from 'react';

const useForm = initialVal => {
    const [state, setState] = useState(initialVal)

    const handleChange = (event) => {
        setState(event.target.value)
    }
    const reset = () => {
        setState('')
    }
    return [state, handleChange, reset]
}

export default useForm;