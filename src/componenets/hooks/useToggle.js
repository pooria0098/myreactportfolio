import {useState} from 'react';

const useToggle = initialVal => {
    const [state, setState] = useState(initialVal)
    // const toggle = () => {
    //     setState(!state)
    // }
    return [state, setState]
}

export default useToggle;